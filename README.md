# The Complete Node.js Developer Course

Artifacts from [Andrew Mead's awesome Node.js course](https://www.udemy.com/the-complete-nodejs-developer-course-2).

If you are looking for an affordable way to learn Node.js, this is a great option! The course covers a
nice variety of projects including a CLI-based app (because CLIs are cool), simple web application
integrated with 3rd-party APIs, a fairly complete REST API with MongoDB backend, and a more involved
web application including a nice sampling of front-end work. It also keeps things real by covering
basic DevOps workflows for production deployment (Git, Heroku).

If you want to learn Node.js or just round out your knowledge, it's an affordable option covering
core functionality, the NPM ecosystem, Express, MongoDB and Mongoose, socket.io, the Handlebars and
Mustache templating engines, and a handful of NPM modules useful in daily life. He also covers more
advanced topics including debugging, TDD with Jest, async (promises chaining, async/await), and
authentication using JWT.

![Certificate of Completion](https://gitlab.com/deadlysyn/complete-nodejs/raw/a2e01d8ef44245aef02afb3d0e91c5d8d55dc582/certNodejs.jpg)
